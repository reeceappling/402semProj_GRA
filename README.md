# NE402 Semester Project

Author  | Due Date
------------- | -------------
Reece Appling  | 11/17/2020

[Node.js](https://nodejs.org/en/download/) and [Python](https://www.python.org/downloads/) are required to run this program.

It is also recommended to have either a file unzipper like [7zip](), or some implementation of [git](https://git-scm.com/), in order to most easily download the source code.
## Installation

Download this repository to its own folder, either by [Unzipping this file](https://gitlab.com/kamikazi3728/402semProj_GRA/-/archive/master/402semProj_GRA-master.zip), or by [Cloning this git link](https://gitlab.com/kamikazi3728/402semProj_GRA.git)

In a terminal window, navigate to the local repository, then install all required Node.js modules with

```bash
npm install
```

A [Python implementation](https://pypi.org/project/iapws/) of standards from [IAPWS](http://www.iapws.org/release.html), also must be installed. To do so, run the following:

```bash
pip install iapws
```

Lastly, due to differences in file structure between systems, a small change must be made in **userSettings.json**, specifically the following line (line 2)

```json
"pythonDirectory":"C:/Python38/python",
```
In the above, "C:/Python38/python" should be changed to "YOUR_DIRECTORY", where "YOUR_DIRECTORY" is the folder that contains python.exe or your OS equivalent.

For example, I can find python.exe in C:/Python38/python, which results in the default value.

## Usage

If you are in the project directory (the directory containing project.js), you can simply use
```bash
node project.js
```
or, in a more general form, from any directory (*not* recommended/tested), you can use
```bash
node PROJECT_DIRECTORY_PATH/project.js
```

## TOTALLY UPDATE THE README//----------------------------------------------

## License
[UnLicense](https://unlicense.org/)